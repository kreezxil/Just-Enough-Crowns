
package net.mcreator.justenoughcrowns.creativetab;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.item.ItemStack;
import net.minecraft.creativetab.CreativeTabs;

import net.mcreator.justenoughcrowns.item.ItemGoldcrown;
import net.mcreator.justenoughcrowns.ElementsJustEnoughCrowns;

@ElementsJustEnoughCrowns.ModElement.Tag
public class TabJustEnoughCrowns extends ElementsJustEnoughCrowns.ModElement {
	public TabJustEnoughCrowns(ElementsJustEnoughCrowns instance) {
		super(instance, 24);
	}

	@Override
	public void initElements() {
		tab = new CreativeTabs("tabjustenoughcrowns") {
			@SideOnly(Side.CLIENT)
			@Override
			public ItemStack getTabIconItem() {
				return new ItemStack(ItemGoldcrown.helmet, (int) (1));
			}

			@SideOnly(Side.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
	public static CreativeTabs tab;
}
