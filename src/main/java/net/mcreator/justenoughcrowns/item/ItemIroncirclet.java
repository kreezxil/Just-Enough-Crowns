
package net.mcreator.justenoughcrowns.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.Item;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.justenoughcrowns.creativetab.TabJustEnoughCrowns;
import net.mcreator.justenoughcrowns.ElementsJustEnoughCrowns;

@ElementsJustEnoughCrowns.ModElement.Tag
public class ItemIroncirclet extends ElementsJustEnoughCrowns.ModElement {
	@GameRegistry.ObjectHolder("justenoughcrowns:ironcirclethelmet")
	public static final Item helmet = null;
	@GameRegistry.ObjectHolder("justenoughcrowns:ironcircletbody")
	public static final Item body = null;
	@GameRegistry.ObjectHolder("justenoughcrowns:ironcircletlegs")
	public static final Item legs = null;
	@GameRegistry.ObjectHolder("justenoughcrowns:ironcircletboots")
	public static final Item boots = null;
	public ItemIroncirclet(ElementsJustEnoughCrowns instance) {
		super(instance, 2);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("IRONCIRCLET", "justenoughcrowns:iron_circlet_", 50, new int[]{2, 5, 6, 4}, 4,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("item.armor.equip_iron")),
				0.4f);
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("ironcirclethelmet")
				.setRegistryName("ironcirclethelmet").setCreativeTab(TabJustEnoughCrowns.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("justenoughcrowns:ironcirclethelmet", "inventory"));
	}
}
