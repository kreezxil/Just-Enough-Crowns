
package net.mcreator.justenoughcrowns.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcreator.justenoughcrowns.item.ItemSilveringot;
import net.mcreator.justenoughcrowns.block.BlockSilverore;
import net.mcreator.justenoughcrowns.ElementsJustEnoughCrowns;

@ElementsJustEnoughCrowns.ModElement.Tag
public class RecipeSmeltsilver extends ElementsJustEnoughCrowns.ModElement {
	public RecipeSmeltsilver(ElementsJustEnoughCrowns instance) {
		super(instance, 11);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockSilverore.block, (int) (1)), new ItemStack(ItemSilveringot.block, (int) (1)), 5F);
	}
}
