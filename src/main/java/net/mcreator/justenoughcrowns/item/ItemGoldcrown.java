
package net.mcreator.justenoughcrowns.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.Item;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.justenoughcrowns.creativetab.TabJustEnoughCrowns;
import net.mcreator.justenoughcrowns.ElementsJustEnoughCrowns;

@ElementsJustEnoughCrowns.ModElement.Tag
public class ItemGoldcrown extends ElementsJustEnoughCrowns.ModElement {
	@GameRegistry.ObjectHolder("justenoughcrowns:goldcrownhelmet")
	public static final Item helmet = null;
	@GameRegistry.ObjectHolder("justenoughcrowns:goldcrownbody")
	public static final Item body = null;
	@GameRegistry.ObjectHolder("justenoughcrowns:goldcrownlegs")
	public static final Item legs = null;
	@GameRegistry.ObjectHolder("justenoughcrowns:goldcrownboots")
	public static final Item boots = null;
	public ItemGoldcrown(ElementsJustEnoughCrowns instance) {
		super(instance, 5);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("GOLDCROWN", "justenoughcrowns:gold_crown_", 25, new int[]{2, 5, 6, 3}, 25,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("item.armor.equip_gold")),
				0.1f);
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("goldcrownhelmet")
				.setRegistryName("goldcrownhelmet").setCreativeTab(TabJustEnoughCrowns.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("justenoughcrowns:goldcrownhelmet", "inventory"));
	}
}
