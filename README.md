This mod adds crowns and silver ore! It was made with MCreator. I tried to follow some tutorials, but the tutorials were harder to follow than simply pointing and clicking with MCreator. The 1.16.5 version of this will be normal java code, but first it will be MCreator code. The source is all open if someone is feeling frisky enough to convert it and do a pr. I am following an Udemy course that is easy to understand. [https://skl.sh/3y4bxU4](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fskl.sh%252f3y4bxU4), and [https://www.udemy.com/course/make-a-minecraft-mod-minecraft-modding-for-beginners-116/learn/lecture/22496344](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.udemy.com%252fcourse%252fmake-a-minecraft-mod-minecraft-modding-for-beginners-116%252flearn%252flecture%252f22496344) after I'm done I'll refactor the 1.16.5 version of this.

[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fkreezcraft.com%252fjustenoughcrowns)

If you want a server setup using Just Enough Crowns with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fkreezcraft.com%252fjustenoughcrowns) and receive 25% off your first month as a new customer using the code kreezxil

### Notes

Everything should be oreDicted, I made sure to set that flag in the recipes.

### Items & Recipes

 See the [Images above for a full list of what this mod adds.](https://www.curseforge.com/minecraft/mc-mods/just-enough-crowns/screenshots)

### TODO:

*   \* Convert 1.12.2 version to proper Forge code
*   \* Convert 1.14.4 version to proper Forge code
*   \* Convert 1.15.2 version to proper Forge code
*   \* Convert 1.16.5 version to proper Forge code

### Permissions:

It's MIT Licensed, so you don't need to ask, just add it and go!
